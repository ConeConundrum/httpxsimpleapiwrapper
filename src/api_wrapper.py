from httpx import AsyncClient, Response
from typing import Optional, Union
from pydantic import BaseModel, Field


class ResponseModel(BaseModel):
    data: dict = Field(default_factory=dict)  # Empty dict for default
    status_code: int
    headers: dict = Field(default_factory=dict)  # Empty dict for default


class SimpleApiWrapper:

    def __init__(self, *, host: str, port: Optional[int] = None, response_pydantic: bool = False):
        self.host = host
        self.port = port
        self._client = AsyncClient()
        self._url = self._form_url()
        self.response_pydantic = response_pydantic

    def _form_url(self) -> str:
        """Url creation method"""
        return f'http://{self.host}:{self.port}' if self.port else f'http://{self.host}'

    async def post(
            self,
            path: str,
            *,
            json: Optional[dict] = None,
            data: Optional[dict] = None,
            params: Optional[dict] = None,
            cookies: Optional[dict] = None,
            auth: Optional[dict] = None,
            headers: Optional[dict] = None
    ) -> Union[ResponseModel, Response]:
        """
        Async post api call method
        :param path:
        :param data:
        :param json:
        :param params:
        :param cookies:
        :param auth:
        :param headers:
        :return: ResponseModel or httpx Response switch response_pydantic
        """

        async with self._client as client:
            response = await client.post(self._url + path, params=params, json=json, data=data,
                                         cookies=cookies, auth=auth, headers=headers)
        if self.response_pydantic:
            return self._process_response(response)
        return response

    async def get(
            self,
            path: str,
            *,
            params: Optional[dict] = None,
            cookies: Optional[dict] = None,
            auth: Optional[dict] = None,
            headers: Optional[dict] = None
    ) -> Union[ResponseModel, Response]:
        """
        Async get api call method
        :param path:
        :param params:
        :param cookies:
        :param auth:
        :param headers:
        :return: ResponseModel or httpx Response switch response_pydantic
        """

        async with self._client as client:
            response = await client.get(self._url + path, params=params, cookies=cookies, auth=auth, headers=headers)
        if self.response_pydantic:
            return self._process_response(response)
        return response

    async def put(
            self,
            path: str = None,
            *,
            json: Optional[dict] = None,
            data: Optional[dict] = None,
            params: Optional[dict] = None,
            cookies: Optional[dict] = None,
            auth: Optional[dict] = None,
            headers: Optional[dict] = None
    ) -> Union[ResponseModel, Response]:
        """
        Async put api call method
        :param path:
        :param json:
        :param data:
        :param params:
        :param cookies:
        :param auth:
        :param headers:
        :return: ResponseModel or httpx Response switch response_pydantic
        """

        async with self._client as client:
            response = await client.put(self._url + path, json=json, params=params, data=data,
                                        cookies=cookies, auth=auth, headers=headers)
        if self.response_pydantic:
            return self._process_response(response)
        return response

    async def delete(
            self,
            path: str = None,
            *,
            params: Optional[dict] = None,
            cookies: Optional[dict] = None,
            auth: Optional[dict] = None,
            headers: Optional[dict] = None
    ) -> Union[ResponseModel, Response]:
        """
        Async delete api call method
        :param path:
        :param params:
        :param cookies:
        :param auth:
        :param headers:
        :return: ResponseModel or httpx Response switch response_pydantic
        """

        async with self._client as client:
            response = await client.delete(self._url + path, params=params, cookies=cookies, auth=auth, headers=headers)
        if self.response_pydantic:
            return self._process_response(response)
        return response

    @staticmethod
    def _process_response(response: Response) -> ResponseModel:
        """
        Additional response processing with pydantic models
        :param response: httpx Response
        :return: pydantic response module
        """
        return ResponseModel(
            data=response.json(),
            status_code=response.status_code,
            headers=response.headers,
        )
